﻿# Créé par Jacoillot Victor, le 25/10/2019 
import csv
import argparse
import os

liste=[]

def CSV_Entrer(file):

    if(os.path.exists(file) is True):

        with open(file,newline='') as csvfile:
            lecteur = csv.DictReader(csvfile, delimiter='|')
            for row in lecteur:
                liste.append(row)
    else:
        print("Erreur: fichier inexistant !")
        exit(1)


def CSV_Sortie(outfile):

    with open(outfile, 'w', newline='') as newfile:

        colonne = ['Adresse du titulaire', 'Nom', 'Prenom','Immatriculation',
        'Date de l\'immatriculation', 'Vin','Marque', 'Denomination_commerciale',
        'Couleur','Carrosserie', 'Categorie', 'Cylindree','Energie','Places',
        'Poids','Puissance', 'Type', 'Variante', 'Version'
        ]

        copy = csv.DictWriter(newfile, colonne, delimiter=';')
        copy.writeheader()

        for row in liste:
            rows = row['type_variante_version'].split(',')

            copy.writerow({'Adresse du titulaire' : row['address'],
                           'Nom' : row['name'],
                           'Prenom' : row['firstname'],
                           'Immatriculation' : row['immat'],
                           'Date de l\'immatriculation' : row['date_immat'],
                           'Vin' : row['vin'],'Marque' : row['marque'],
                           'Denomination_commerciale' : row['denomination'],
                           'Couleur' : row['couleur'],
                           'Carrosserie' : row['carrosserie'],
                           'Categorie' : row['categorie'],
                           'Cylindree' : row['cylindree'],
                           'Energie' : row['energy'],'Places' : row['places'],
                           'Poids' : row['poids'],'Puissance' : row['puissance'],
                           'Type' : rows[0],'Variante' : rows[1],'Version' : rows[2]
            })

parser = argparse.ArgumentParser()
parser.add_argument('file',help='Example : auto.csv')
parser.add_argument('outfile',help='Example : sortie.csv')
args = parser.parse_args()


if __name__ == "__main__":
    CSV_Entrer(args.file)
    CSV_Sortie(args.outfile)